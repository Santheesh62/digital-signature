import React, { useRef, useEffect, useState } from "react";
import { Col, Container, Row, Button } from "react-bootstrap";
import WebViewer from "@pdftron/webviewer";
import "gestalt/dist/gestalt.css";
import "./PrepareDocument.css";

const PrepareDocument = () => {
  const [instance, setInstance] = useState(null);
  const [values, setValues] = useState({
    uploadButton: "Upload",
    saveButton: "Save",
    downloadButton: "",
  });
  const [dropPoint, setDropPoint] = useState(null);
  const [annotManager, setAnnotatManager] = useState(null);

  const viewer = useRef(null);
  const filePicker = useRef(null);

  // if using a class, equivalent of componentDidMount
  useEffect(() => {
    WebViewer(
      {
        path: "webviewer",
        disabledElements: [
          "textSelectButton",
          "viewControlsButton",
          "selectToolButton",
          "redoButton",
          "panToolButton",
          "leftPanelButton",
          "ribbons",
          "toggleNotesButton",
          "searchButton",
          "menuButton",
          "searchButton",
          "menuButton",
          "rubberStampToolGroupButton",
          "stampToolGroupButton",
          "fileAttachmentToolGroupButton",
          "calloutToolGroupButton",
          "eraserToolButton",
          "undoButton",
          "textSignaturePanelButton",
          "imageSignaturePanelButton",
          "inkSignaturePanelButton",
          "colorPalette",
          "zoomOutButton",
          "zoomInButton",
          "zoomOverlayButton",
          "moreButton",
          "header",
        ],
      },

      viewer.current
    ).then((instance) => {
      const { iframeWindow } = instance;

      // select only the view group
      instance.setToolbarGroup("toolbarGroup-View");
      setInstance(instance);

      const iframeDoc = iframeWindow.document.body;
      iframeDoc.addEventListener("dragover", dragOver);
      iframeDoc.addEventListener("drop", (e) => {
        drop(e, instance);
      });

      filePicker.current.onchange = (e) => {
        const file = e.target.files[0];

        if (file) {
          if (file.size < 12582912) {
            instance.loadDocument(file);
          } else {
            alert("File Size Should less than 12MB or Upload file properly");

            setValues({ ...values, uploadButton: "Upload" });
          }
        } else {
          alert("Upload file properly");
          setValues({ ...values, uploadButton: "Upload" });
        }
      };
      const { annotManager, Annotations } = instance;
      setAnnotatManager(annotManager);

      // select only the insert group
      instance.setToolbarGroup("toolbarGroup-Insert");
      const normalStyles = (widget) => {
        if (widget instanceof Annotations.TextWidgetAnnotation) {
          return {
            "background-color": "#a5c7ff",
            color: "white",
          };
        } else if (widget instanceof Annotations.SignatureWidgetAnnotation) {
          return {
            border: "1px solid #a5c7ff",
          };
        }
      };

      annotManager.on(
        "annotationChanged",
        (annotations, action, { imported }) => {
          if (imported && action === "add") {
            annotations.forEach(function (annot) {
              if (annot instanceof Annotations.WidgetAnnotation) {
                Annotations.WidgetAnnotation.getCustomStyles = normalStyles;
              }
            });
          }
        }
      );
    });
  }, []);

  const { uploadButton } = values;
  const download = () => {
    instance.downloadPdf(true);
  };

  const dragOver = (e) => {
    e.preventDefault();
    return false;
  };

  const drop = (e, instance) => {
    const { docViewer } = instance;
    const scrollElement = docViewer.getScrollViewElement();
    const scrollLeft = scrollElement.scrollLeft || 0;
    const scrollTop = scrollElement.scrollTop || 0;
    setDropPoint({ x: e.pageX + scrollLeft, y: e.pageY + scrollTop });
    e.preventDefault();
    return false;
  };

  return (
    <div className={"prepareDocument"}>
      <Container display="flex" direction="row" flex="grow">
        <Col span={12}>
          <div className="p-2">
            {/* {filePicker.current == null ? displayPdf() : " "} */}
          </div>
          <div className="webviewer" ref={viewer}></div>
          <div class="dropzone">
            <input type="file" ref={filePicker} />

            <h3>Drag and Drop here file here</h3>
            <p>or</p>
            <Button
                  onClick={() => {
                    if (filePicker) {
                      if (uploadButton === "Upload") {
                        filePicker.current.click();
                        setValues({ ...values, uploadButton: "Save" });
                      } else if (uploadButton === "Save") {
                        setValues({ ...values, uploadButton: "Download" });
                      } else if (uploadButton === "Download") {
                        download();
                      }
                    }
                  }}
                  accessibilityLabel="upload a document"
                  text="Upload"
                >
                  {uploadButton}
                </Button>
                
          </div>
          <Container padding={3}>
            <Row>
              <Col>
              <h6>The file should be pdf format and the size should not exceed 12MB</h6>
              </Col>
            </Row>
          </Container>
        </Col>
      </Container>
    </div>
  );
};

export default PrepareDocument;
