# Description
**Digital Signature** 
The below languages ans libraries also used    
   - node v14.16.0
   - npm  v6.14.11
   - yarn v1.22.4(Installing the required packages)
   - Reactjs(JavaScript library)
   - HTML, CSS and JavaScript (Integrated in Reactjs)
   - React-Bootstrap



# Digital Signature
   ## Step 1:
  The below page is to add file using **Upload** button or **Drag and Drop** the PDF file within 12MB size.  
  ![Uploading Page of PDF](https://gitlab.com/Santheesh62/digital-signature/-/raw/master/img/Upload.png)
    
   ## Step 2: 
     When click the **Add New Signature** in top of PDF view it displays the **Signature Box**.
     After signed click **Create** button and place the signature in the required place or use **Clear** button remove signature 
   ![Uploading Page of PDF](https://gitlab.com/Santheesh62/digital-signature/-/raw/master/img/add-signature.png)
   ## Step 3:
     Paste the Signed Signature in the required palce.
     After pasted the signature click the save button to add the signature in pdf file
   ![Uploading Page of PDF](https://gitlab.com/Santheesh62/digital-signature/-/raw/master/img/paste%20signature.png)

   ## Step 4:
   Finally Click **Download** button to download the signed pdf file.     
   ![Uploading Page of PDF](https://gitlab.com/Santheesh62/digital-signature/-/raw/master/img/download.png)

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm install`
### `npm run start`
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


### `npm run test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).


### `yarn build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
